var winningIds=["NA","NA","NA"];
var crossZero=['B','B','B','B','B','B','B','B','B'];
var moves=0;
var is_winner_set=-1;
function checkWinner(){
    if(crossZero[0]==crossZero[1] && crossZero[1]==crossZero[2]){
        winningIds[0]="bd0";
        winningIds[1]="bd1";
        winningIds[2]="bd2";
        if(crossZero[0]=='X'){
            return 1;
        }
        else if(crossZero[0]=='0'){
            return 2;
        }
    }
    else if(crossZero[0]==crossZero[3] && crossZero[0]==crossZero[6]){
        winningIds[0]="bd0";
        winningIds[1]="bd3";
        winningIds[2]="bd6";
        if(crossZero[0]=='X'){
            return 1;
        }
        else if(crossZero[0]=='0'){
            return 2;
        }
    }
    else if(crossZero[0]==crossZero[4] && crossZero[0]==crossZero[8]){
        winningIds[0]="bd0";
        winningIds[1]="bd4";
        winningIds[2]="bd8";
        if(crossZero[0]=='X'){
            return 1;
        }
        else if(crossZero[0]=='0'){
            return 2;
        }
    }
    else if(crossZero[1]==crossZero[4] && crossZero[1]==crossZero[7]){
        winningIds[0]="bd1";
        winningIds[1]="bd4";
        winningIds[2]="bd7";
        if(crossZero[1]=='X'){
            return 1;
        }
        else if(crossZero[1]=='0'){
            return 2;
        }
    }
    else if(crossZero[2]==crossZero[5] && crossZero[2]==crossZero[8]){
        winningIds[0]="bd2";
        winningIds[1]="bd5";
        winningIds[2]="bd8";
        if(crossZero[2]=='X'){
            return 1;
        }
        else if(crossZero[2]=='0'){
            return 2;
        }
    }
    else if(crossZero[2]==crossZero[4] && crossZero[2]==crossZero[6]){
        winningIds[0]="bd2";
        winningIds[1]="bd4";
        winningIds[2]="bd6";
        if(crossZero[2]=='X'){
            return 1;
        }
        else if(crossZero[2]=='0'){
            return 2;
        }
    }
    else if(crossZero[6]==crossZero[7] && crossZero[6]==crossZero[8]){
        winningIds[0]="bd6";
        winningIds[1]="bd7";
        winningIds[2]="bd8";
        if(crossZero[6]=='X'){
            return 1;
        }
        else if(crossZero[8]=='0'){
            return 2;
        }
    }
    else{
        if(moves==9){
            return 0;
        }
    }
}
function player_move(id){
    var hasSet=document.getElementById(id).style.backgroundImage.length
    if(hasSet==0 && is_winner_set!=1){
        var x=document.getElementById("player").selectedIndex;
        if(x==1){
            document.getElementById(id).style.backgroundImage="url('icons/cross.png')";
            document.getElementById(id).style.backgroundSize="80px 80px";
            document.getElementById(id).style.backgroundRepeat="no-repeat";
            document.getElementById(id).style.backgroundPosition="center";
            if(id=="bd0")
                crossZero[0]='X';
            else if(id=="bd1")
                crossZero[1]='X';
            else if(id=="bd2")
                crossZero[2]='X';
            else if(id=="bd3")
                crossZero[3]='X';
            else if(id=="bd4")
                crossZero[4]='X';
            else if(id=="bd5")
                crossZero[5]='X';
            else if(id=="bd6")
                crossZero[6]='X';
            else if(id=="bd7")
                crossZero[7]='X';
            else if(id=="bd8")
                crossZero[8]='X';
            moves++;
        }
        else if(x==2){
            document.getElementById(id).style.backgroundImage="url('icons/zero.png')";
            document.getElementById(id).style.backgroundSize="80px 80px";
            document.getElementById(id).style.backgroundRepeat="no-repeat";
            document.getElementById(id).style.backgroundPosition="center";
            if(id=="bd0")
                crossZero[0]='0';
            else if(id=="bd1")
                crossZero[1]='0';
            else if(id=="bd2")
                crossZero[2]='0';
            else if(id=="bd3")
                crossZero[3]='0';
            else if(id=="bd4")
                crossZero[4]='0';
            else if(id=="bd5")
                crossZero[5]='0';
            else if(id=="bd6")
                crossZero[6]='0';
            else if(id=="bd7")
                crossZero[7]='0';
            else if(id=="bd8")
                crossZero[8]='0';
            moves++;
        }
        else{
            alert("Choose a Player")
        }
    }
    if(moves>=5){
        var win=checkWinner();
        if(win!=0){
            is_winner_set=1;
            for(let i=0;i<3;i++){
                document.getElementById(winningIds[i]).style.border="5px solid blue";
            }
            if(win==1)
                document.getElementById("result").innerHTML="Congratulations, Player 1";
            else{
                document.getElementById("result").innerHTML="Congratulations, Player 2";
            }
            setTimeout(function(){
                location.reload();
            },5000)
        }
    }
}